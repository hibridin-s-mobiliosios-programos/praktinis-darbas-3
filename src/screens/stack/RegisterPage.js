import React, {
    useState
} from 'react';

import {
    View,
    Text,
    Alert,
    TextInput,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import auth from '@react-native-firebase/auth';

import CustomButton from '../../components/customButton';
import CustomTextInput from '../../components/customTextButton';
import { NavigationHelpersContext } from '@react-navigation/native';

const registerErrorAlert = (message) => {
    Alert.alert(
        "",
        messageText = message.code === 'auth/email-already-in-use' ? 'Email already in use!' :
            message.code === 'auth/invalid-email' ? 'Email address is invalid!' : "Something went wrong!",
        [ { text: 'OK', onPress: () => console.log("OK Pressed")} ]
    );
}

function RegisterPage({ navigation: { navigate } }) {
    // Email / Password states
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // Password error handlers
    const [passwordError, setPasswordError] = useState(false);

    const passwordErrorChange = (value) => {
        if(value.length < 4) {
            setPassword(true);
            setPasswordError(true);
        } else {
            setPassword(value);
            setPasswordError(false);
        }
    }

    const registerUser = () => {
        const user = {
            email: email,
            password: password
        }

        console.log('Registration data:\r\n\tEmail: ' + user.email + "\r\n\tPassword: " + user.password);

        auth()
            .createUserWithEmailAndPassword(email, password)
            .then(() => {
                alert("Registration successful!");
                NavigationHelpersContext('LoginPage');
            })
            .catch(error => {
                console.log(error);
                registerErrorAlert(error);
            });
    }

    return(
        <SafeAreaView style={[ styles.SafeAreaView ]}>
            <View style={[ styles.header ]}>
                <Text style={[ styles.text ]}>Register Page...</Text>
            </View>

            <View style={[ styles.body ]}>
                <CustomTextInput
                    title="Email"
                    placeholder="Enter your email..."
                    onChangeText={(email) => setEmail(email)}
                />

                <CustomTextInput
                    title="Password"
                    placeholder="Enter your password..."
                    onChangeText={(password) => passwordErrorChange(password)}
                />

                {passwordError ? (
                    <View>
                        <Text>Password must be longer than 3 characters!</Text>
                    </View>
                ) : null }

                <CustomButton
                    title="Register"
                    onPress={registerUser}
                    disabled={passwordError === true ? true : false}
                />

                <TouchableOpacity onPress={ () => navigate('LoginPage')}>
                    <Text>Already have an account? Press here!</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    header: {
        height: 65,
        backgroundColor: 'slategray',
        justifyContent: 'center',
        alignItems: 'center'
    },

    body: {
        padding: 20
    },

    text: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    hint: {
        fontSize: 12,
        fontStyle: 'italic'
    }
});

export default RegisterPage;