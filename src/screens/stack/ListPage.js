import React, {
    useState,
    useEffect
} from 'react';

import {
    View,
    Text,
    Button,
    FlatList,
    StyleSheet,
    ScrollView,
} from 'react-native';

import {
    database,
    firebase
} from '@react-native-firebase/database';

import auth from '@react-native-firebase/auth';

import referece from '../../firebaseConfig';

function ListPage({navigation: {navigate}}) {
    // Advertisement object list state
    const [advertisements, setAdvertisements] = useState([]);

    // Render update function
    useEffect(() => {
        referece.ref('advertisements')
            .on('value', (snapshot) => {
                setAdvertisements([]);
                snapshot.forEach((child) => {
                    const advertisement = {
                        id: child.val().id,
                        name: child.val().name,
                        description: child.val().description,
                        price: child.val().price
                    };
                    setAdvertisements(emptyArray => [...emptyArray, advertisement]);
                })
                console.log(JSON.stringify(advertisements));
            })
    }, []);

    const signOut = () => {
        auth().signOut().then(() => {
            console.log('Sign out')
            navigate('LoginPage')
        })
    }

    const deletePost = (id) => {
        referece.ref('advertisements/' + id).remove();
    }

    return(
        <View style={styles.container}>
            <Text style={styles.title}>Advertisements</Text>

            <View style={{flexDirection: 'row'}}>

            </View>
            <Button
                  onPress={() => signOut()}
                  title="Sign out"
                />

                <Button
                  onPress={() => navigate('CreatePage')}
                  title="Create"
                />
            <FlatList
                style={ styles.list }
                data={advertisements}
                renderItem={(item) => {
                    return(
                        <View style={styles.row}>
                            <Text style={styles.rowTitle} onPress={() => navigate('AdvertisementPage', item.item)}>{item.item.name} - {item.item.price} {'\u20AC'}</Text>
                            <Button title='Delete post' style={{ alignSelf: 'center'}} onPress={() => deletePost(item.item.id)} />
                        </View>
                    )
                }}
                keyExtractor={item => item.id}
                scrollEnabled={true}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#898989'
    },

    title: {
        paddingTop: 30,
        paddingBottom: 20,
        fontSize: 20,
        color: 'black',
        textAlign: 'center',
        fontWeight: 'bold'
    },

    row: {
        padding: 20,
        backgroundColor: '#ffffff',
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 5
    },

    rowTitle: {
        color: 'black',
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 15
    },

    list: {
        padding: 20,
    }
})

export default ListPage;