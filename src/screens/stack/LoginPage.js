import React, {
    useState,
    useEffect
} from 'react';

import {
    Text,
    View,
    Alert,
    StyleSheet,
    SafeAreaView,
    TouchableOpacity
} from 'react-native';

import auth from '@react-native-firebase/auth';

import CustomButton from '../../components/customButton'
import CustomTextInput from '../../components/customTextButton';

const loginErrorAlert = (message) => {
    Alert.alert(
        "",
        messageText =
            message.code === 'auth/invalid-email' ? 'Wrong Email Address...' : 
            message.code === 'auth/wrong-password' ? 'Wrong Password...' : 
            message.code === 'auth/user-not-found' ? 'User doesn\'t exist!' : 'User does not exist!',
        [ { text: 'OK', onPress: () => console.log("OK pressed") } ]
    )
}


function LoginPage({navigation: {navigate}}) {
    // Email / Password states
    const [email, setEmail] = useState(null);
    const [password, setPassword] = useState(null);

    // Email / Password error handlers
    const [emailError, setEmailError] = useState('false');
    const [passwordError, setPasswordError] = useState('false');

    // Render update function
    useEffect(() => {
        const user = auth().onAuthStateChanged(currentUser => {
            console.log(currentUser);
            if(currentUser !== null)
                navigate('ListPage');
            else
                navigate('LoginPage');
        });

        return user;
    });

    const passwordChecker = (value) => {
        if(value.length < 1) {
            setPassword(value);
            setPasswordError(true);
        } else {
            setPassword(value);
            setPasswordError(false);
        }
    }

    const loginUser = () => {
        if(email === "" || email === null)
            return;

        if(password === "" || password === null)
            return;

        auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                console.log('Login was succesfull.\r\n\tEmail: ' + email + '\r\n\tPassword: ' + password);
            }).catch(error => {
                loginErrorAlert(error);
            })
    }

    return(
        <SafeAreaView style={[ styles.safeAreaView ]}>
            <View style={[ styles.header ]}>
                <Text style={[ styles.text ]}>Login Page...</Text>
            </View>

            <View style={[ styles.body ]}>
                <CustomTextInput
                    title="Email"
                    placeholder="Enter your email..."
                    onChangeText={(email) => setEmail(email)}
                />

                <CustomTextInput
                    title="Password"
                    placeholder="Enter your password..."
                    onChangeText={(password) => passwordChecker(password)}
                />

                <CustomButton
                    title="Login"
                    onPress={loginUser}
                    disabled={false}
                />

                <TouchableOpacity onPress={() => navigate('RegisterPage')}>
                    <Text>Don't have an account? Press here!</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    safeAreaView: {
        flex: 1,
    },

    header: {
        height: 65,
        backgroundColor: 'slategray',
        justifyContent: 'center',
        alignItems: 'center'
    },

    body: {
        padding: 20
    },

    text: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    hint: {
        fontSize: 12,
        fontStyle: 'italic'
    }
});

export default LoginPage;