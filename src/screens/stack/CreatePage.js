import React, {
    useState
} from 'react';

import {
    View,
    Text,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import {
    firebase
} from '@react-native-firebase/database';

import reference from '../../firebaseConfig';

function CreatePage() {
    // Advertisement variable states
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');

    const ref = reference.ref('/advertisements');

    handleSubmit = () => {
        const newReference = ref.push();
        newReference.set({
            id: newReference.key,
            name: name,
            description: description,
            price: price,
            createBy: firebase.auth().currentUser.email
        })
        .then(() => console.log('Advertisement created!'));

        setName('');
        setDescription('');
        setPrice('');
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Create an Advertisement:</Text>
            <View styles={styles.inputContainer}>
                <TextInput
                    style={styles.input}
                    value={name}
                    placeholder="Name..."
                    placeholderTextColor="#b2b2b2"
                    onChangeText={(text) => setName(text)}
                />

                <TextInput
                    style={styles.input}
                    value={description}
                    placeholder="Description..."
                    placeholderTextColor="#b2b2b2"
                    onChangeText={(text) => setDescription(text)}
                />

                <TextInput
                    style={styles.input}
                    value={price}
                    placeholder="Price..."
                    placeholderTextColor="#b2b2b2"
                    onChangeText={(text) => setPrice(text)}
                />
            </View>

            <View style={styles.addButtonContainer}>
                <TouchableOpacity onPress={handleSubmit}>
                    <View style={styles.addButton}>
                        <Text style={styles.addButtonText}>
                            Add
                        </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 2
    },

    title: {
        color: 'black',
        paddingTop: 30,
        paddingBottom: 20,
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold'
    },

    inputContainer: {
        backgroundColor: '#000000',
        borderTopColor: '#ededed',
        borderTopWidth: 1,
        flexDirection: 'row',
        height: 60,
        width: 60
    },

    input: {
        height: 40,
        padding: 7,
        color: 'black',
        backgroundColor: '#FFFFFF',
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 5
    },

    addButtonContainer: {
        flex: 4,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },

    addButton: {
        width: 120,
        height: 40,
        backgroundColor: '#6cc900',
        marginLeft: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 20
    },

    addButtonText: {
        fontSize: 24,
        lineHeight: 24
    }
});

export default CreatePage;