import React, {
    useEffect,
    useState
} from "react";

import {
    Text,
    View,
    Button,
    TextInput,
    ScrollView,
    StyleSheet,
    SafeAreaView,
    Alert,
} from 'react-native';

import reference from "../../firebaseConfig";

const alertUser = (message) => {
    Alert.alert(
        "Success",
        message,
        [
            {
                text: 'Ok', 
            }
        ]
    )
}

function AdvertisementPage({route, navigation: {navigate}}) {
    const item = route.params;

    const [name, updateName] = useState(item.name);
    const [description, updateDescription] = useState(item.description);
    const [price, updatePrice] = useState(item.price);

    updateAdvertisement = () => {
        if(name === '' || name === null)
            return;
        if(description === '' || description === null)
            return;
        if(price === '' || price === null)
            return;

        reference.ref('advertisements/' + item.id)
            .update({
                name: name,
                description: description,
                price: price
            })
            .then(() => {
                alertUser('Advertisement updated!');
                navigate('ListPage');
            });
    }

    return(
        <SafeAreaView style={{flex: 1}}>
            <ScrollView style={[ styles.scroll ]}>
                <View style={[ styles.header ]}>
                    <Text style={[ styles.headerText ]}>{ item.name }</Text>
                </View>

                <View style={[ styles.body ]}>
                    <Text style={[ styles.bodyText ]}>Description: { item.description } </Text>
                    <Text style={[ styles.bodyText ]}>Price: { item.price } </Text>
                    <Button title="EDIT" onPress={updateAdvertisement}/>
                </View>

                <View style={[ styles.body ]}>
                    <TextInput
                        style={styles.input}
                        value={name}
                        placeholder="Name..."
                        placeholderTextColor="#b2b2b2"
                        onChangeText={(text) => updateName(text)}
                    />

                    <TextInput
                        style={styles.input}
                        value={description}
                        placeholder="Description..."
                        placeholderTextColor="#b2b2b2"
                        onChangeText={(text) => updateDescription(text)}
                    />

                    <TextInput
                        style={styles.input}
                        value={price}
                        placeholder="Price..."
                        placeholderTextColor="#b2b2b2"
                        onChangeText={(text) => updatePrice(text)}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    scroll: {
        flexDirection: 'column',
        backgroundColor: 'peachpuff'
    },

    header: {
        height: 65,
        backgroundColor: 'peru',
        justifyContent: 'center',
        alignItems: 'center'
    },

    headerText: {
        fontSize: 22,
        fontWeight: 'bold'
    },

    body: {
        padding: 20,
    },

    bodyText: {
        fontSize: 16,
        color: 'black',
        marginBottom: 10
    },

    input: {
        height: 40,
        padding: 7,
        color: 'black',
        backgroundColor: '#FFFFFF',
        borderColor: '#ddd',
        borderWidth: 1,
        borderRadius: 10,
        marginBottom: 5
    },
});

export default AdvertisementPage;