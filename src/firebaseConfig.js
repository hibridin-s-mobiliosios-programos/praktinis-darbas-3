import { firebase } from '@react-native-firebase/database';

// Firebase Realtime database reference URL
const reference = firebase.app().database("https://react-native-project-31019-default-rtdb.europe-west1.firebasedatabase.app/");

export default reference;