import React from 'react';
import {
  Text,
  View,
  Button,
  StyleSheet,
  SafeAreaView
} from 'react-native';

import {
  NavigationContainer
} from '@react-navigation/native';

import {
  createNativeStackNavigator
} from '@react-navigation/native-stack';

import LoginPage from './src/screens/stack/LoginPage';
import RegisterPage from './src/screens/stack/RegisterPage';
import ListPage from './src/screens/stack/ListPage';
import CreatePage from './src/screens/stack/CreatePage';
import AdvertisementPage from './src/screens/stack/AdvertisementPage';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginPage">
        <Stack.Screen
          name="LoginPage"
          component={LoginPage}
          options={{ title: 'Login page', headerShown: false }}
        />
        
        <Stack.Screen
          name="RegisterPage"
          component={RegisterPage}
          options={{ title: 'Register page', headerShown: false }}
        />
        
        <Stack.Screen
          name="ListPage"
          component={ListPage}
          options={{
            title: 'List page',
            headerShown: false
          }}
        />
        
        <Stack.Screen
          name="CreatePage"
          component={CreatePage}
          options={{
            title: 'Create page',
          }}
        />

        <Stack.Screen
          name="AdvertisementPage"
          component={AdvertisementPage}
          options={{
            title: 'Advertisement Page',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}



export default App;
