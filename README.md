# Praktinis Darbas 3

<div align="center">
  <b>Studentas</b>: Emilis Žvirblys<br><b>Grupė</b>: PI20C<br><b>Dėstytojas</b>: J. Zailskas
</div>

## Requirements

Create a **React Native** project using Android / iOS. The project in question must use a database (SQLite, Firebase, etc).
